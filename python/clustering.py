from typing import Dict, Union, Callable, List

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import entropy
from scipy.spatial.distance import pdist, squareform
from scipy.cluster.hierarchy import linkage, dendrogram
from sklearn.neighbors import kneighbors_graph
import feather

import pymc3 as pm
from theano import tensor as T
import theano

from pymc_tools import BayesianSurvival, rv_generator, CovariateSet, Model, Result
from pymc_tools.utilities import pereira_sternberg

from shared import tricontext_probabilities, endpoints, entities


def jensen_shannon_divergence(p, q):
    n_bins = 1000
    # find common bins
    _, bins = np.histogram(np.concatenate((p, q)), n_bins)
    # create discrete distribution
    dist_p, _ = np.histogram(p, bins)
    dist_q, _ = np.histogram(q, bins)

    # https://github.com/scipy/scipy/issues/4080 suggests
    # e = entropy(x, y) + np.log(bins[1] - bins[0])
    # but this makes the entropy negative, which cannot be true

    dist_p = dist_p / np.linalg.norm(dist_p, ord=1)
    dist_q = dist_q / np.linalg.norm(dist_q, ord=1)
    m = 0.5 * (dist_p + dist_q)
    return 0.5 * (entropy(dist_p, m) + entropy(dist_q, m))


def jensen_shannon_distance(p, q):
    return np.sqrt(jensen_shannon_divergence(p, q))


def posterior_difference(p, q,
                         alpha=0.05,
                         theta_null=0):
    diff = p-q
    mean = np.mean(diff)
    hpd = pm.stats.hpd(diff, alpha)
    p = pereira_sternberg(diff, theta_null)
    return mean, hpd[0], hpd[1], p


def pdist_distribution(trace_array: np.ndarray,
                       func,
                       index=None):
    # trace_array is shaped (n_samples, shape of var). We assume shape[1] as the number of distributions.
    # Move this axis to first dimension, squeeze all the rest (assumedly trace_array is only 2-dim, but more would work)
    x = np.moveaxis(trace_array, 1, 0).reshape(trace_array.shape[1], -1)
    d = pdist(x, func)
    if index is not None:
        d = squareform(d)
        d = pd.DataFrame(d, index=index, columns=index)
    return d


def distribution_linkage(trace_array,
                         method):
    dist_matrix = pdist_distribution(trace_array, jensen_shannon_distance)
    return linkage(dist_matrix, method=method, optimal_ordering=True)


def plot_distribution_clusters(trace_array,
                               labels,
                               method,
                               ax=None):
    assert trace_array.shape[1] == len(labels)
    if ax is None:
        ax = plt.gca()

    z = distribution_linkage(trace_array, method)

    def llf(n):
        if n < len(labels):
            return labels[n]
        return ""

    dendrogram(z,
               orientation="left",
               distance_sort=True,
               leaf_label_func=llf,
               ax=ax)

