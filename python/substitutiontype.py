from typing import List, Dict, Callable, Union, Tuple
import os
import copy
import pickle
from collections import OrderedDict
from pathlib import Path

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as stats
import feather

import pymc3 as pm
from pymc3 import gelman_rubin
from theano import tensor as T

from pymc_tools.analysis import df_summary

from shared import as_ggx_category, tricontext_probabilities, hotspot_probabilites, hotspot_protein_mutations


def ggx_codon_bias(codon: int,
                   entity: str,
                   observed_mutations: pd.Series) -> pd.DataFrame:

    observed_mutations = as_ggx_category(observed_mutations)

    n_mutations = 6 # a GGx has six possible non-synonymous mutations

    tricontext_expected = tricontext_probabilities()
    tricontext_expected = tricontext_expected.loc[
        (tricontext_expected["entity"] == entity) & (tricontext_expected["codon"] == codon),
        "expected probability"
    ]

    with pm.Model() as model:

        #weights_mutational_bias = pm.Gamma(name="weights mutational bias", alpha=0.01, beta=0.01, shape=n_mutations)
        p_mutations = pm.Dirichlet(name="p mutations",
                                   a=np.repeat(1/n_mutations, n_mutations))

        prevalence = pm.Categorical(name="prevalence",
                                    p=p_mutations,
                                    observed=observed_mutations.cat.codes)

        #ratio = pm.Deterministic("ratio", p_mutations / tricontext_probabilities - 1.0)
        relative_difference = pm.Deterministic("relative difference",
                                               (p_mutations - tricontext_expected) / tricontext_expected)
        absolute_difference = pm.Deterministic("absolute difference",
                                                p_mutations - tricontext_expected)

        sample_scale = 1#00
        n_samples = 1000 * sample_scale
        n_tune = 500 * sample_scale
        n_burn = 500 * sample_scale
        n_jobs = 2#10
        trace = pm.sample(draws=n_samples + n_burn,
                          tune=n_tune,
                          njobs=n_jobs)

        return df_summary(trace,
                          varnames=["p mutations", "relative difference", "absolute difference"],
                          start=n_burn,
                          theta_null=0)

        ###
        #trace = trace[n_burn:]
        #pm.plot_posterior(trace, varnames=["ratio"])
        #plt.show()
        #pm.plot_posterior(trace, varnames=["relative difference"])
        #plt.show()

        #print(gelman_rubin(trace))
        #pm.traceplot(trace)
        #plt.show()


if __name__ == "__main__":

    # tricontext_probabilities().pipe(feather.write_dataframe, "../kras-expected-codon-12-13.feather")
    # exit()
    hotspot_probabilites().pipe(feather.write_dataframe, "../kras-expected-hotspots.feather")
    exit()

    summaries = []
    for source, source_label in [("../kras-observed-codon-12-13.feather", "own data"),
                                 ("../kras-biocportal-codon-12-13.feather", "biocPortal")]:
        df = feather.read_dataframe(source)
        for entity in ["CRC", "NSCLC"]:

            for codon in [12, 13]:
                mask = (df["entity"] == entity) & (df["codon"] == codon)
                summary = ggx_codon_bias(codon,
                                         entity,
                                         df.loc[mask, "mutation"])
                var_idc = summary.index.str.extract("__(\d+)", expand=False).astype("int")
                summary = summary.assign(
                    entity=entity,
                    codon=codon,
                    source=source_label,
                    variable=summary.index.str[:-3],
                    mutation=hotspot_protein_mutations(codon)[var_idc].values
                )
                summaries.append(summary)

    summaries = pd.concat(summaries, axis=0)  # type: pd.DataFrame

    summaries = summaries.sort_values(by=["entity", "codon", "source"]).reset_index(drop=True)
    feather.write_dataframe(summaries, "../mutational_bias.feather")
    summaries.to_csv("../mutational_bias.csv", sep=";", decimal=",")