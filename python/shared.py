# from typing import List, Dict, Callable, Union, Tuple

import pandas as pd
import numpy as np
import feather

#    protein  cDNA baseSubstitution cDNA_std context_std tricontext   ras
#  1    G12S 34G>A              G>A      C>T         CCA    C[C>T]A  KRAS
#  2    G12R 34G>C              G>C      C>G         CCA    C[C>G]A  KRAS
#  3    G12C 34G>T              G>T      C>A         CCA    C[C>A]A  KRAS
#  4    G12D 35G>A              G>A      C>T         ACC    A[C>T]C  KRAS
#  5    G12A 35G>C              G>C      C>G         ACC    A[C>G]C  KRAS
#  6    G12V 35G>T              G>T      C>A         ACC    A[C>A]C  KRAS

#  7    G13S 37G>A              G>A      C>T         CCA    C[C>T]A  KRAS
#  8    G13R 37G>C              G>C      C>G         CCA    C[C>G]A  KRAS
#  9    G13C 37G>T              G>T      C>A         CCA    C[C>A]A  KRAS
# 10    G13D 38G>A              G>A      C>T         GCC    G[C>T]C  KRAS
# 11    G13A 38G>C              G>C      C>G         GCC    G[C>G]C  KRAS
# 12    G13V 38G>T              G>T      C>A         GCC    G[C>A]C  KRAS


def mutations_12_13_df():
    return feather.read_dataframe("../kras-exon-12-13-possible-mutations.feather")


def mutations_hotspots_df(codons=None):
    df = feather.read_dataframe("../kras-hotspots-possible-mutations.feather")
    if codons:
        if codons is int:
            codons = [codons]
        df = df.loc[df["codon"].isin(codons)]
    return df


def hotspot_categories() -> pd.DataFrame:
    return mutations_12_13_df()["protein"].str[-1].drop_duplicates()


def hotspot_protein_mutations(codon: int) -> pd.Series:
    df = mutations_hotspots_df(codon)
    return df["protein"].reset_index(drop=True)


def signatures_df() -> pd.DataFrame:
    sigs = feather.read_dataframe("../signatures.nature2013.feather")
    sigs = sigs.set_index("signature")
    sigs.index = sigs.index.str.replace(".", " ")
    return sigs


def as_ggx_category(s: pd.Series):
    last_letter = s.str[-1]
    categorized = pd.Series(pd.Categorical(last_letter, categories=hotspot_categories()))
    return categorized.dropna()


def tricontext_probabilities(codons):
    return expected_relative_distribution(mutations_hotspots_df(codons), "codon", "protein")


def modified_tricontext_probabilities(codons):
    return expected_relative_distribution(mutations_hotspots_df(codons), "codon", "protein",
                                          manual_probs={"G12S": 0.,
                                                        "G13R": 0.,
                                                        "G13A": 0.,
                                                        "G13S": 0.,
                                                        "G13V": 0.,
                                                        "Q61*": 0.,
                                                        "Q61E": 0.,
                                                        "Q61P": 0.})


def expected_relative_distribution(data,
                                   by,
                                   unique,
                                   manual_probs: dict = None):
    if by:
        grouped_df = data.groupby(by)
    else:
        grouped_df = data.groupby(pd.Series(1, index=data.index))

    signature_contributions_per_entity = {
        "CRC": {
            "Signature 1B": 0.171,
            "Signature 6": 0.367,
            "Signature 10": 0.293,
            "Signature R3": 0.169
        },
        "NSCLC": {
            "Signature 1B": 0.091,
            "Signature 2": 0.128,
            "Signature 4": 0.61,
            "Signature 5": 0.096,
            "Signature R2": 0.075
        }
    }
    signatures = signatures_df()

    # def expected(tricontext):
    #     if tricontext is None:
    #         return 0.
    #     return sum(
    #         [rel_contrib * signatures.loc[sig, tricontext]
    #          for sig, rel_contrib in signature_contributions.items()]
    #     )

    def expected(tricontext):
        series = pd.Series()
        if tricontext is None:
            return series
        for sig, rel_contrib in signature_contributions.items():
            series[sig] = rel_contrib * signatures.loc[sig, tricontext]
        return series

    df = pd.DataFrame()
    for entity, signature_contributions in signature_contributions_per_entity.items():
        for id_, group in grouped_df:
            tricontexts = group["tricontext"]
            expected_relative = tricontexts.apply(expected)

            # allow manual manipulation
            if manual_probs:
                for key, rel in manual_probs.items():
                    mask = group["protein"] == key
                    if mask.any():
                        expected_relative[mask] = rel

            sums = expected_relative.sum(axis=1)
            norm = sums.sum()
            expected_relative["expected probability"] = sums
            expected_relative /= norm

            df = df.append(
                pd.concat([group, expected_relative],
                          axis=1).
                assign(group_id=id_,
                       entity=entity)
            )

    grouping = []
    if by is list:
        grouping.extend(by)
    elif by:
        grouping.append(by)
    grouping.append("entity")
    if unique is list:
        grouping.extend(unique)
    else:
        grouping.append(unique)
    df = df.groupby(grouping).sum(axis=1)

    return df


def hotspot_probabilites():
    df = mutations_hotspots_df()
    df = expected_relative_distribution(df, None, "codon")
    return df.groupby(["entity", "codon"]).sum(axis=1).select_dtypes(np.number).reset_index()


def entities():
    return ["NSCLC",
            "CRC"]


def endpoints():
    os = ["OS", "OS reached"]
    # noinspection PyUnusedLocal
    os_pall = ["OS palliative", "OS reached"]
    os_iv = ["OS stage IV", "OS reached"]
    ttf1 = ["TTF1", "TTF1 reached"]
    ttf1_pall = ["TTF1 palliative", "TTF1 palliative reached"]
    # noinspection PyUnusedLocal
    ttf2 = ["TTF2", "TTF2 reached"]
    ttf_platin = ["TTF Platin", "TTF Platin reached"]

    return [os, os_iv, ttf1_pall, ttf_platin]

tricontext_probabilities([12, 13, 61]).reset_index().to_feather("../kras-expected-codon-12-13.feather")
modified_tricontext_probabilities([12, 13, 61]).reset_index().to_feather("../kras-expected-codon-12-13-modified.feather")
hotspot_probabilites().reset_index().to_feather("../kras-expected-hotspots.feather")