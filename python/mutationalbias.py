from typing import Dict

import pandas as pd
import numpy as np
import feather
import copy

import pymc3 as pm
from pymc3 import gelman_rubin
from theano import tensor as T

from pymc_tools import BayesianSurvival, rv_generator
from pymc_tools.utilities import diagnose_model_specification

from substitutiontype import tricontext_probabilities


ref_level = "G12C"
mutation_column = "RAS protein level"

probs_df = tricontext_probabilities()
codon12_variants = probs_df.query("codon == 12")["protein"].drop_duplicates()

data = feather.read_dataframe("../kras-codon-12-full-data.feather")
# Clean categories, removing two-base-exchange G12F
data[mutation_column] = data[mutation_column].cat.set_categories(codon12_variants)
data = data.dropna(axis=0, subset=[mutation_column])


def survival_models() -> Dict[str, Dict[str, BayesianSurvival.Model]]:
    surv = BayesianSurvival(data)

    os = ["OS", "OS reached"]
    osPall = ["OS palliative", "OS reached"]
    osIV = ["OS stage IV", "OS reached"]
    ttf1 = ["TTF1", "TTF1 reached"]
    ttf2 = ["TTF2", "TTF2 reached"]
    ttfPlatin = ["TTF Platin", "TTF Platin reached"]

    endpoints = [os]#, osIV, ttf1, ttfPlatin]
    entities = ["NSCLC", "CRC"]

    covariates_complete = {
        "CRC": [mutation_column, "age at dx", "UICC", "M", "sex", "right_left"],
        "NSCLC": [mutation_column, "age at dx", "UICC", "M", "sex", "EGFR, ALK or ROS1"]
    }
    covariates_withmissing = {
        "CRC": {"G": rv_generator(pm.Categorical, p=[0.05, 0.85, 0.1]) },
        "NSCLC": None
    }
    ref_level_dict = {
        mutation_column: ref_level
    }

    models = {}

    for entity in entities:
        models[entity] = surv.multivariate_models(
            survival_columns_list=endpoints,
            covariates_fully_observed=covariates_complete[entity],
            covariates_with_distribution=covariates_withmissing[entity],
            ref_level_dict=ref_level_dict
        )

    return models


def extend_model(model: BayesianSurvival.Model,
                 entity: str,
                 method: str = "add"):
    df = probs_df.query("codon == 12 & entity == @entity")

    with model.pymc_model:

        a = pm.HalfNormal(name="scale", sd=10)
        b = pm.Normal(name="shift", mu=0, sd=10)

        def weight():
            for row in df.index:
                p = df.loc[row, "expected probability"]
                variant = df.loc[row, "protein"]
                coef = model.covariate_set.coefficient(mutation_column, variant)
                if coef is None:
                    # ref level
                    coef = T.as_tensor_variable(0)

                T.printing.Print("coefficient for {}".format(variant))(coef - b)
                T.printing.Print("weight for {}, p {}".format(variant, p))(p + a * (coef - b))
                if method == "add":
                    yield p + a * (coef - b)
                else:
                    yield p * T.exp(a*(coef-b))

        weights = T.stack(list(weight()))
        probs = weights / T.sum(weights)
        T.printing.Print("probs")(probs)

        pm.Categorical(name="observed frequencies",
                       p=probs,
                       observed=data[mutation_column].cat.codes)

#with pm.Model() as model:
#    extend_model(model, "NSCLC")
#    trace = pm.sample(1000)
#    pm.summary(trace)
#    exit()

models = survival_models()
for entity, model_dict in models.items():
    for endpoint, model in model_dict.items():
        print("Model for entity {} and endpoint {} with {} patients".
              format(entity, endpoint, model.data_set.n_patients))
        out_directory = "../plots/bayes/{}/{}".format(entity, endpoint)

        with copy.deepcopy(model.pymc_model):
            init_trace = pm.sample(draws=2000, tune=1000)
        start_values = {name: init_trace[name][-1]
                        for name in model.pymc_model.unobserved_RVs}

        extend_model(model, entity)
        #with model.pymc_model:
         #   step=pm.Metropolis()
        sample_scale = 5000
        result = model.sample(out_directory=out_directory,
                              n_tune=5*sample_scale,
                              n_burn=5*sample_scale,
                              n_samples=10*sample_scale,
                              n_jobs=10,
                              start=start_values)
        print(model.pymc_model.named_vars.keys())
        pm.summary(result.trace,
                   #varnames=["beta categorical", "shift", "scale"],
                   to_file=out_directory + "/" + endpoint + "-pm-summary")
        result.analyse()



