from typing import Dict, Union, Callable, List

import pandas as pd
import numpy as np
import feather

from joblib import Parallel, delayed

import pymc3 as pm
from theano import tensor as T
import theano

from pymc_tools import BayesianSurvival, rv_generator, CovariateSet, Model, Result
from pymc_tools.utilities import diagnose_model_specification

from shared import tricontext_probabilities, endpoints, entities


base_out_directory = "../plots/bayes/mutationalbias"
wt_level = "WT"
ref_level = wt_level
mutation_column = "ras_mutation"

probs_df = tricontext_probabilities()
codon12_variants = probs_df.query("codon == 12")["protein"].drop_duplicates()
# levels (index = code, value = category) used for the mutation column
mutation_levels = codon12_variants.append(pd.Series(wt_level))
# levels (index = position, value = category) used for positional assignment of variables (coefficients)
mutation_levels_positional = mutation_levels[mutation_levels != ref_level].reset_index(drop=True)

data = (feather.read_dataframe("../kras-wt-and-codon-12-full-data.feather").
        assign(ras_mutation=lambda x: x["RAS protein level"].cat.add_categories([wt_level]),
               G=lambda x: x["G"].astype("category")).
        fillna({"ras_mutation": wt_level}).
        # clean categories. Note: removes G12F!
        assign(ras_mutation=lambda x: x[mutation_column].cat.set_categories(mutation_levels)).
        dropna(axis=0, subset=[mutation_column])
        )  # type: pd.DataFrame

print("Theano flags:", theano.config.optimizer, theano.config.exception_verbosity)


class PerEntityModel:

    def __init__(self,
                 type_: str,
                 entity: str,
                 survival_columns: List[str],
                 covariates_with_distribution: Dict[str, Callable],
                 covariates_fully_observed: Union[None, List[str]],
                 ref_level_dict: Dict):

        self.type_ = type_
        self.entity = entity
        self.init_trace = None
        self.survival_columns = survival_columns
        self.covariates_with_distribution = covariates_with_distribution
        self.covariates_fully_observed = covariates_fully_observed
        self.ref_level_dict = ref_level_dict
        self.df = data.query("entity == @entity")

        self.model = None
        self.ras_coefficients = None
        self.discard_models = {"baseline": [],
                               "short": [],
                               "full": []}

    def construct_main(self):
        """Construct main model.

        Call under combined-parent model context
        """

        surv = BayesianSurvival(self.df)
        with pm.Model(name=self.entity):
            self.model = surv.create_multivariate_model(
                        survival_columns=self.survival_columns,
                        covariates_with_distribution=self.covariates_with_distribution,
                        covariates_fully_observed=self.covariates_fully_observed,
                        ref_level_dict=self.ref_level_dict
                    )
        print("Constructing model for {} with {} patients ({} with survival data)".
              format(self.entity, self.df.shape[0], self.model.data_set.n_patients))

    def construct_discard(self,
                          n_baseline: int = 1,
                          n_short: int = 1,
                          n_full: int = 1):
        """Construct models to discard.
        """
        if self.model is None:
            raise RuntimeError("Must call construct_main before construct_discard")
        if pm.Model.get_contexts():
            raise RuntimeError("Must not call construct_discard within a model context!")

        # Construct models on shortened data to retrieve starting points and then discard
        short_surv_df = self.model.data_set.data.sample(50, random_state=1)
        surv_full = BayesianSurvival(self.df)
        surv_short = BayesianSurvival(short_surv_df)
        dataset_short = surv_short.build_dataset(self.survival_columns,
                                                 self.covariates_fully_observed)

        for mode in n_baseline * ["baseline"] + n_short * ["short"] + n_full * ["full"]:
            with pm.Model(name=self.entity):
                if mode == "baseline":
                    covariate_set = surv_short.build_covariate_set(dataset_short, {}, [])
                    model = surv_short.model_object(dataset_short, covariate_set)
                elif mode == "short":
                    covariate_set = surv_short.build_covariate_set(
                        dataset_short,
                        covariates_with_distribution=self.covariates_with_distribution,
                        covariates_fully_observed=self.covariates_fully_observed,
                        ref_level_dict=self.ref_level_dict
                    )
                    model = surv_short.model_object(dataset_short, covariate_set)
                else:
                    covariate_set = surv_full.build_covariate_set(
                        self.model.data_set,
                        covariates_with_distribution=self.covariates_with_distribution,
                        covariates_fully_observed=self.covariates_fully_observed,
                        ref_level_dict=self.ref_level_dict
                    )
                    model = surv_full.model_object(self.model.data_set, covariate_set)
            self.discard_models[mode].append(model)

    def add_ras_covariate(self,
                          ras_common_beta):

        def add_covariate_group(model, rvs, df):
            mut = df[mutation_column]
            one_hot = np.eye(mutation_levels.size)[mut.cat.codes]
            one_hot = one_hot[:, mut.cat.categories != ref_level]
            ids = [(mutation_column, level) for level in mut.cat.categories if level != ref_level]
            group = CovariateSet.Group(CovariateSet.categorical,
                                       covariates=one_hot,
                                       coefficients=rvs,
                                       ids=ids)
            model.covariate_set.groups.append(group)

        with self.model:
            if self.type_ == "combined":
                self.ras_coefficients = ras_common_beta + pm.Normal(name="ras entity-specific correction",
                                                                    mu=0,
                                                                    sd=0.5,
                                                                    shape=len(mutation_levels_positional))
            elif self.type_ == "common":
                self.ras_coefficients = ras_common_beta
            else:  # self.type_ == "separate":
                self.ras_coefficients = BayesianSurvival.default_coefficient_for("ras entity-specific beta",
                                                                                 mutation_levels_positional)

            pm.Deterministic("ras beta", self.ras_coefficients)
            add_covariate_group(self.model, self.ras_coefficients, self.model.data_set.data)

        for mode, models in self.discard_models.items():
            for model in models:
                with model:
                    add_covariate_group(model,
                                        BayesianSurvival.default_coefficient_for("ras beta",
                                                                                 mutation_levels_positional),
                                        model.data_set.data)
                    # model.covariate_set.print = True

    def entangle(self):
        expected = (probs_df.
                    query("codon == 12 & entity == @self.entity").
                    set_index("protein").
                    loc[codon12_variants, "expected probability"].
                    values
                    )
        print("Expected for {}: {} ({})".format(self.entity, expected, expected.shape))

        with self.model:
            # v2
            # scale = pm.StudentT(name="scale", nu=3)
            # loc = pm.StudentT(name="loc", nu=3)

            # def transformation(x):
            #     return T.exp(scale * (x - loc))

            # v3
            # scale = pm.Gamma(name="scale", alpha=1, beta=1)
            #
            # def transformation(x):
            #     return scale * T.exp(x)
            #
            # selectional_bias = pm.Deterministic("selectional bias", transformation(self.ras_coefficients))
            #
            # weights = expected * selectional_bias

            # v4
            scale = pm.Gamma(name="scale", alpha=1, beta=1)
            ras_prob_simplex = pm.Dirichlet(name="ras prob simplex",
                                            a=T.exp(self.ras_coefficients),
                                            shape=expected.shape[0])
            selectional_bias = pm.Deterministic("selectional bias", scale * ras_prob_simplex)
            weights = expected + selectional_bias

            probs = pm.Dirichlet(name="probs",
                                 a=weights,
                                 shape=expected.shape[0])

            observations = (self.df.
                            query("ras_mutation != @wt_level")
                            [mutation_column].
                            cat.codes
                            )

            print("Relative KRAS codon 12 mutation distribution for {} with {} patients".
                  format(self.entity, observations.shape[0]))

            pm.Categorical(name="observed frequencies",
                           p=probs,
                           observed=observations)

    def build(self):
        self.model.build()

    def store_start_values(self, mode: str):
        print("Generating start values for {} ({})".format(self.entity, mode))
        model = self.discard_models[mode].pop(0)
        model.build()
        start_values = model.extract_start_values(self.init_trace) if self.init_trace is not None else None
        # model.diagnose_specification()
        self.init_trace = model.plain_sample(n_samples=1000, n_tune=500, start=start_values, random_seed=1)

    def start_values(self):
        return self.model.extract_start_values(self.init_trace, with_prefix=True)


class CombinedModel(Model):

    def __init__(self,
                 out_directory,
                 type_: str,
                 entities: List[str],
                 survival_columns: List[str],
                 covariates_with_distribution_dict: Dict,
                 covariates_fully_observed_dict: Dict,
                 ref_level_dict: Dict
                 ):
        super().__init__(pymc_model=pm.Model(name="combined"))

        self.entity_models = [
            PerEntityModel(type_,
                           entity,
                           survival_columns=survival_columns,
                           covariates_fully_observed=covariates_fully_observed_dict[entity],
                           covariates_with_distribution=covariates_with_distribution_dict[entity],
                           ref_level_dict=ref_level_dict
                           )
            for entity in entities
        ]
        self.out_directory = out_directory
        self.endpoint = survival_columns[0]
        self.type_ = type_

        ras_common_beta = None
        if self.type_ in ["common", "combined"]:
            with self.pymc_model:
                # create ras betas common to entities
                ras_common_beta = BayesianSurvival.default_coefficient_for("ras common beta",
                                                                           mutation_levels_positional)

        for entity_model in self.entity_models:
            with self:
                entity_model.construct_main()
            entity_model.construct_discard()
            entity_model.store_start_values("baseline")
            entity_model.add_ras_covariate(ras_common_beta)
            entity_model.store_start_values("short")
            # entity_model.store_start_values("full")

            entity_model.entangle()
            entity_model.build()

    def sample(self):
        start_values = {key: value
                        for model in self.entity_models
                        for key, value in model.start_values().items()
                        }

        # does not work because some input is missing (problem with nested models?)
        # diagnose_model_specification(self.pymc_model, start_values)

        varnames_specific_shaped = [
            "ras beta",
            "selectional bias",
            "probs"]
        varnames_specific_single = [
            #"loc",
            "scale"]
        varnames_combined = []

        if self.type_ == "combined":
            varnames_specific_shaped += ["ras entity-specific correction"]
            varnames_combined += ["ras common beta"]

        var_shape = mutation_levels_positional.shape[0]

        def varnames():
            for model in self.entity_models:
                for n in varnames_specific_shaped + varnames_specific_single:
                    yield model.model.pymc_model.name_for(n)
            for n in varnames_combined:
                yield self.pymc_model.name_for(n)

        def variables_dfs():
            for model in self.entity_models:
                yield pd.DataFrame({
                    "variable name": np.repeat(varnames_specific_shaped, var_shape),
                    "mutation": np.tile(mutation_levels_positional, [len(varnames_specific_shaped)]),
                    "entity": model.entity
                })
                yield pd.DataFrame({
                    "variable name": varnames_specific_single,
                    "mutation": "",
                    "entity": model.entity
                })
            yield pd.DataFrame({
                "variable name": np.repeat(varnames_combined, var_shape),
                "mutation": np.tile(mutation_levels_positional, [len(varnames_combined)]),
                "entity": "meta"
            })

        result = Result(out_directory=self.out_directory,
                        result_id=self.endpoint)
        result.sample_from_model(self,
                                 n_tune=25000,
                                 n_burn=25000,
                                 n_samples=50000,
                                 n_jobs=10,
                                 varnames_to_store=list(varnames()),
                                 start=start_values)
        summary = pd.concat([
            pd.concat(list(variables_dfs()), axis=0).reset_index(drop=True),
            result.df_summary_pereira(variable_name_as_column="variable id")
            ], axis=1)  # type: pd.DataFrame

        print(summary)
        return result, summary


def start_model(multivariate, type_):
    if multivariate:
        covariates_complete = {
            "CRC": ["age at dx", "UICC", "M", "sex", "right_left"],
            "NSCLC": ["age at dx", "UICC", "M", "sex", "EGFR, ALK or ROS1"]
        }
        covariates_withmissing = {
            "CRC": {"G": rv_generator(pm.Categorical, p=[0.05, 0.85, 0.1])},
            "NSCLC": None
        }
    else:
        covariates_complete = {
            "CRC": None,
            "NSCLC": None,
        }
        covariates_withmissing = {
            "CRC": None,
            "NSCLC": None
        }

    ref_level_dict = {
        mutation_column: ref_level
    }

    results = {}
    summaries = pd.DataFrame()
    out_directory = "{}/{}/{}".format(base_out_directory, "multi" if multivariate else "uni", type_)
    for endpoint in endpoints()[:1]:
        model = CombinedModel(out_directory,
                              type_,
                              entities(),
                              survival_columns=endpoint,
                              covariates_with_distribution_dict=covariates_withmissing,
                              covariates_fully_observed_dict=covariates_complete,
                              ref_level_dict=ref_level_dict
                              )
        result, summary = model.sample()
        results[result.result_id] = result
        summaries = summaries.append(summary, ignore_index=True)

    feather.write_dataframe(summaries, out_directory + "/summary.feather")
    return results


if __name__ == "__main__":
    import argparse as ap
    parser = ap.ArgumentParser()
    parser.add_argument("type", help="model type", type=str)
    args, _ = parser.parse_known_args()

    for multivariate in [False, True]:
        start_model(multivariate, args.type)