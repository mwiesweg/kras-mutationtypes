
rasCorrelatePrognostic <- function(data = data_full)
{
  sourceVariables(rasLabels())
  data %<>% mutate(`age at dx` = if_else(`age at dx` == 0, na_dbl, `age at dx`))
  
  bind_rows(
    rasCorrelatePrognosticWithPhosphoMarkers(data),
    rasCorrelatePrognosticWithP53(data),
    rasCorrelatePrognosticWithEpi(data),
    rasCorrelatePrognosticWithTNM(data)
  ) %>% 
    #unite(entity_test, entity, method) %>%
    select(marker, entity, method, p) %>%
    mutate(marker = str_replace(marker, "p-AKTₛ₄₇₃", "p-AKT"),
           entity = lookup_chr(entity_titles, entity, default = identity)) %>%
    format_numbers(vars(p), 3) %>%
    spread(method, p, fill="") %>%
    # give coefficient only if significant / trend
    mutate(`Correlation (Spearman)` = 
             if_else(`Correlation (Spearman): p` == "", "",
                     if_else(as.numeric(`Correlation (Spearman): p`) < 0.1,
                             str_c(`Correlation (Spearman): p`, "\ncoef: ",`Correlation (Spearman): coefficient`),
                             `Correlation (Spearman): p`))
    ) %>%
    select(-starts_with("Correlation (Spearman): ")) %>%
    arrange(match(entity, c("NSCLC", "CRC")),
            match(marker, c("age at dx", "sex", "T", "N", "M", "G", "UICC", "right_left", "p53 mutation", "p-AKT", "p-ERK"))) %>%
    clipboard()
}

rasCorrelationFigure <- function(data = data_full)
{
  sourceVariables(rasThemes())
  sourceVariables(rasFactorLabels())
  
  m <- ras_prognostic_correlate(data, vars(M), 
                                  func_per_entity = partial(ras_abstract_plot_sequential,
                                                            file_base_name=NULL),
                                  func_per_entity_and_marker = ras_abstract_kruskal_and_spearman,
                                  return_plots = T)
  
  markers <- vars(`p-AKTₛ₄₇₃`, `p-ERK`, `p-p70S6K`)
  
  ras_prognostic_correlate(data, markers, 
                           func_per_entity = NULL,
                           func_per_entity_and_marker = ras_abstract_mean)
  pm <- ras_prognostic_correlate(data, markers, 
                                 func_per_entity = 
                                   function(subset, entity, marker, factor)
                                   {
                                     ras_abstract_plot_boxplot(subset, entity, marker, factor, file_base_name=NULL)
                                     #ras_abstract_plot_violin(subset, entity, marker, factor, file_base_name=NULL)
                                   },
                                 func_per_entity_and_marker = ras_abstract_kruskal_and_spearman,
                                 return_plots = T
  )
  
  p53 <- ras_prognostic_correlate(data,
                                  markers=vars(p53),
                                  func_per_entity = partial(ras_abstract_plot_binary,
                                                            file_base_name=NULL,
                                                            marker_grouping=label_mutation_boolean),
                                  func_per_entity_and_marker = ras_abstract_chisq,
                                  return_plots = T)
  
  bind_rows(map(list(m, pm, p53), "df")) %>% print
  
  p53_legend <- get_legend(pluck(p53, "plots", "nsclc") + base_theme)
  p53_theme <- base_theme + theme(legend.position="none")
  p53_row <- plot_grid(pluck(p53, "plots", "nsclc") + ggtitle("C: NSCLC") + p53_theme,
                       pluck(p53, "plots", "crc") + ggtitle("CRC") + ylab("") + p53_theme,
                       pluck(p53, "plots", "both CRC and NSCLC") + ylab("") + ggtitle("Both Entities") + p53_theme,
                       p53_legend,
                       ncol = 4, rel_widths = c(1,1,1,0.3))
  
  plot_grid(pluck(pm, "plots", "crc") + ggtitle("A: CRC") + base_theme,
            pluck(m, "plots", "crc") + ggtitle("B: CRC") + base_theme,
            p53_row,
            nrow = 3) %>%
    save_pdf("../plots/correlations", "Suppl.Fig.8",
             width=dinAWidth(5)*1.5, height = dinAHeight(5)*3)
}

rasCorrelatePrognosticWithTNM <- function(data = data_full)
{
  ras_prognostic_correlate(data, vars(T, N, M, G, UICC, right_left), 
                           func_per_entity = partial(ras_abstract_plot_sequential,
                                                     file_base_name="tnm"),
                           func_per_entity_and_marker = ras_abstract_kruskal_and_spearman)
}



rasCorrelatePrognosticWithEpi <- function(data = data_full)
{
  sourceVariables(rasFactorLabels())
  bind_rows(
  ras_prognostic_correlate(data, vars(`age at dx`), 
                           func_per_entity = partial(ras_abstract_plot_histograms,
                                                     file_base_name="age"),
                           func_per_entity_and_marker = ras_abstract_aov),
  ras_prognostic_correlate(data, vars(`sex`), 
                           func_per_entity = partial(ras_abstract_plot_binary,
                                                     file_base_name="sex",
                                                     factor_grouping=label_sex),
                           func_per_entity_and_marker = ras_abstract_chisq)
  )
}

rasCorrelatePrognosticWithP53 <- function(data = data_full)
{
  sourceVariables(rasFactorLabels())

  # ras_prognostic_correlate(data, 
  #                          markers=vars(p53), 
  #                          func_per_entity = partial(ras_abstract_plot_sequential,
  #                                                    file_base_name="p53",
  #                                                    factor_grouping=label_mutation_boolean),
  #                          invert_factor_and_marker = T,
  #                          func_per_entity_and_marker = ras_abstract_chisq)
  ras_prognostic_correlate(data,
                           markers=vars(p53, PIK3CA),
                           func_per_entity = partial(ras_abstract_plot_binary,
                                                     file_base_name="mutations",
                                                     marker_grouping=label_mutation_boolean),
                           func_per_entity_and_marker = ras_abstract_chisq)
}

rasCorrelatePrognosticWithPhosphoMarkers <- function(data = data_full)
{
  markers <- vars(`p-AKTₛ₄₇₃`, `p-ERK`, `p-p70S6K`)
  
  ras_prognostic_correlate(data, markers, 
                           func_per_entity = 
                             function(subset, entity, marker, factor)
                             {
                               ras_abstract_plot_quartiles(subset, entity, marker, factor, file_base_name="phosphomarkers")
                               ras_abstract_plot_violin(subset, entity, marker, factor, file_base_name="phosphomarkers-violin")
                             },
                           # CHANGE HERE TO RETRIEVE MEANS for PAPER
                           func_per_entity_and_marker = #ras_abstract_mean
                             ras_abstract_kruskal_and_spearman
  )
}

ras_prognostic_correlate <- function(data, markers, 
                                     factor = quo(`RAS protein level with gene`),
                                     invert_factor_and_marker = F,
                                     func_per_entity = NULL,
                                     func_per_entity_and_marker = NULL,
                                     return_plots = F)
{
  both_entities <- "both CRC and NSCLC"
  entities <- c(ras_short_entities(), both_entities)

  subsets <- list()
  results <- list()
  plots <- list()
  for (entity in entities)
  {
    if (entity == both_entities)
    {
      bind_rows(subsets) %>%
        mutate(!!quo_expr(factor) := base::factor(!!factor)) ->
        subset
      print(levels(subset %>% pull(!!factor)))
    }
    else
    {
      print(entity)
      data %>%
        ras_filter(entity) %>%
        ras_mutatefactor_manualgrouping(!!factor,
                                        entity,
                                        "protein",
                                        withWT = F) %>%
        select(!!factor, !!!markers) ->
      subset
      subsets[[entity]] <- subset
    }
    
    if (invert_factor_and_marker)
    {
      marker_inv <- markers[[1]]
      factor_inv <- vars(!!quo_expr(factor))
      if (!is_null(func_per_entity_and_marker))
      {
        tryCatch(
          subset %>%
            drop_na() %>%
            func_per_entity_and_marker(entity, factor_inv[[1]], marker_inv) %>%
            append_object(results) ->
            results,
          error = function(x) NULL
        )
      }
      
      if (!is_null(func_per_entity))
      {
        subset %>%
          #drop_na() %>%
          func_per_entity(entity, factor_inv, marker_inv) ->
        plots[[entity]]
      }
    }
    else
    {
      if (!is_null(func_per_entity_and_marker))
        map(markers, function(marker)
        {
          tryCatch(
            subset %>%
            drop_na(!!factor, !!marker) %>%
            func_per_entity_and_marker(entity, marker, factor),
            error = function(x) NULL
          )
        })  %>% 
        prepend(results) ->
      results
        
      if (!is_null(func_per_entity))
      {
        subset %>%
          #drop_na() %>%
          func_per_entity(entity, markers, factor) ->
        plots[[entity]]
      }
    }
  }
  
  df <- bind_rows(results)
  if (return_plots)
    list(df=df, plots=plots)
  else
    df
}